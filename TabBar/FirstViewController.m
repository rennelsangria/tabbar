//
//  FirstViewController.m
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "FirstViewController.h"

#import "SecondViewController.h"
@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    self.title = @"Recipes";
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    //Build the array from the plist
    arrayOfRecipes = [[NSArray alloc] initWithContentsOfFile:path];
    
    RecipeObjArray = [[NSMutableArray alloc]init];
    
    
    //using a for loop, take the items in arrayOfRecipes and turn it into recipe objects and add them to the RecipeObjArray
    for(NSDictionary *D in arrayOfRecipes)
    {
        Recipe *o = [[Recipe alloc]init];
        o.recipeName = [D objectForKey:@"Name"];
        o.recipeImage = [D objectForKey:@"Image"];
        o.recipeIngredients = [D objectForKey:@"Description"];
        [RecipeObjArray addObject:o];
    }
    
    
    
    UITableView* tableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    [self.view addSubview:tableView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [arrayOfRecipes count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
 
    
    NSDictionary* infoDictionary = RecipeObjArray[indexPath.row];
    cell.textLabel.text = [infoDictionary valueForKey:@"recipeName"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //NSDictionary* selectedRecipe = RecipeObjArray[indexPath.row];
    //NSLog(@"FirstViewController recipe item was touched");
    
    //SecondViewController* item = [SecondViewController new];
    //item.menuItem = selectedRecipe;
    [self.tabBarController setSelectedIndex:1];
    
    
    //pass in the infoDictionary object
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"FirstViewItemTouched" object:RecipeObjArray[indexPath.row]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
