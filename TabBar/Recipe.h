//
//  Recipe.h
//  RecipeAppUsingTabBar
//
//  Created by Rennel Sangria on 6/8/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject

@property (nonatomic, strong) NSString* recipeName;
@property (nonatomic, strong) NSString* recipeImage;
@property (nonatomic, strong) NSString* recipeIngredients;

@end
