//
//  ViewController.m
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    //create new instances of the first and second view controllers
    FirstViewController* fvc = [FirstViewController new];
    SecondViewController* svc = [SecondViewController new];
    
    NSArray* tabViewControllers = @[fvc,svc];
    
    [self setViewControllers:tabViewControllers];
    
    fvc.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"First" image:nil tag:0];
    svc.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Second" image:nil tag:1];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
