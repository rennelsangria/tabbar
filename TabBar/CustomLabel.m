//
//  CustomLabel.m
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel

-(instancetype)initWithFrame:(CGRect)frame
{
    self= [super initWithFrame:frame];
    
    if (self){
        self.textColor = [UIColor whiteColor];
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
