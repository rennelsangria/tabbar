//
//  FirstViewController.h
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SecondViewController.h"
#import "ViewController.h"
#import "Recipe.h"

@interface FirstViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray* arrayOfRecipes;
    NSMutableArray *RecipeObjArray ;
}

@end
