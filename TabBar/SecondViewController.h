//
//  SecondViewController.h
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "FirstViewController.h"
#import "CustomLabel.h"


@interface SecondViewController : UIViewController
//create a property of an object
@property(nonatomic, strong) NSDictionary* menuItem;


@end
