//
//  UIColor+CustomColors.m
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+(UIColor*)primaryPurpleColor{
    
    return [UIColor colorWithRed:102 green:0 blue:55 alpha:1];
}


@end
