//
//  UIColor+CustomColors.h
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)
+(UIColor*)primaryPurpleColor;

@end
