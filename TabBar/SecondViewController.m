//
//  SecondViewController.m
//  TabBar
//
//  Created by Rennel Sangria on 6/9/15.
//  Copyright (c) 2015 Rennel Sangria. All rights reserved.
//

#import "SecondViewController.h"


@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //use custom color
    self.view.backgroundColor = [UIColor primaryPurpleColor];
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notificationReceived:)   //what we want to do when we receive a notification--call a function
                                                 name:@"FirstViewItemTouched"
                                               object:nil];
}


-(void)notificationReceived:(NSNotification*)notification {
    
    CustomLabel* nameLbl = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 30)];
    nameLbl.text = [notification.object valueForKey: @"recipeName"];
    [self.view addSubview:nameLbl];
    
    UIImageView* imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 300)];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [imgView setImage:[UIImage imageNamed:[notification.object valueForKey:@"recipeImage"]]];
    [self.view addSubview:imgView];
    
    UITextView* txtView = [[UITextView alloc]initWithFrame:CGRectMake(0, 410, self.view.frame.size.width, 200)];
    txtView.textColor = [UIColor brownColor];
    txtView.text = [notification.object valueForKey:@"recipeIngredients"];
    [self.view addSubview:txtView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


//use custom label
//    CustomLabel* nameLbl = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 80, self.view.frame.size.width, 30)];
//    nameLbl.text = [self.informationDictionary valueForKey:@"recipeName"];
//    [self.view addSubview:nameLbl];
//
//    UIImageView* imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, 300)];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [imgView setImage:[UIImage imageNamed:[self.informationDictionary valueForKey:@"recipeImage"]]];
//    [self.view addSubview:imgView];
//
//    UITextView* txtView = [[UITextView alloc]initWithFrame:CGRectMake(0, 410, self.view.frame.size.width, 200)];
//    txtView.textColor = [UIColor brownColor];
//    txtView.text = [self.informationDictionary valueForKey:@"recipeIngredients"];
//    [self.view addSubview:txtView];
